import os
import yaml 
  
# This is to get the directory that the program  
# is currently running in. 
dir_path = os.getcwd()

builds = ['package.json', 'Pipfile', 'pyproject.toml']
children = {}

for root, dirs, files in os.walk(dir_path): 
    for file in files:  
  
        # change the extension from '.mp3' to  
        # the one of your choice. 
        if file in builds:
            location = builds.index(file)
            the_path = os.path.join(root,file)
            children[builds[location]] = []
            folder = os.path.dirname(os.path.relpath(the_path, start = os.curdir))
            children[builds[location]].append(folder)

print(children)

new_jobs = {}
for key in children:
    for fname in children[key]:
        templatename = 'citemplates/'+key+'.yaml'
        yamlfile = open(templatename)
        template = yaml.load(yamlfile)
        yamlfile.close()
        print(template)
        template['script'] = [s.replace('AUTOGENPROJECT', fname) for s in template['script']]
        job_name = template['stub'] + '-' + fname 
        new_jobs[job_name] = template
        new_jobs[job_name].pop('stub')


print(new_jobs)

jobfile=open(r'jobs.yaml','w')
yaml.dump(new_jobs,jobfile)
jobfile.close()

